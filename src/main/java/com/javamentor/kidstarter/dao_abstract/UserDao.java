package com.javamentor.kidstarter.dao_abstract;

import com.javamentor.kidstarter.model.User;
import org.springframework.stereotype.Service;

public interface UserDao extends GenericDao<Long , User> {

}
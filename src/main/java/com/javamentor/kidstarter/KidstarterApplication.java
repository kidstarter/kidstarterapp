package com.javamentor.kidstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KidstarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(KidstarterApplication.class, args);
	}
}
